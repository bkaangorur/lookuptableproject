close all; clear all; clc;

mkoctfile lookup_table_gateway.cpp -o lookup_table_gateway
autoload('createLookupTable',   'lookup_table_gateway.oct');
autoload('requestLookupTable',  'lookup_table_gateway.oct');
autoload('printLookupTable',    'lookup_table_gateway.oct');