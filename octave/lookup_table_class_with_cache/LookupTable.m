%%  Author: Bilge Kaan G�r�r, bkaangorur@hotmail.com, Feb 2021.
%%  Interpolation operation was adapted from the implementation in https://github.com/parsiad/mlinterp

classdef LookupTable < handle
    properties
        database;               %   Previously computed/measured data 
        breakpointList = {};    %   The list of breakpoint data vectors
        cacheList;              %   The list of caches that each holds the last index
        pow_array;
        dimList;                %   The list of dimensions of each breakpoint 
        dim = 0;
    endproperties
    
    methods
        function createLookupTable(this, database, varargin)
            this.database = database;
            this.dim      = length(varargin);
            this.cacheList(1:this.dim) = 1;
            for i=1:this.dim
                this.breakpointList{i} = varargin{i};
                this.dimList(i) = length(varargin{i}(:));
            endfor
        endfunction

        function result = request(this, varargin)
            N               = this.dim;
            z               = zeros(1, N);
            o               = ones(1, N);
            indices         = z;
            weights         = z;
            result          = 0;
            dimList         = this.dimList;
            database        = this.database;
            breakpointList  = this.breakpointList;
            pow_array       = 2.^([1:N]-1);
            cacheList       = this.cacheList;
            
            
            for i=1:N
                breakpoint = breakpointList{i};
                q = varargin{i};
                cache = cacheList(i);
                dim = dimList(i);
                
                %%  Data point is less than left boundary
                if (dim == 1 || q <= breakpoint(1))
                    mid = 1;
                    weight = 1.;
               
               %%  Data point is greater than right boundary
                elseif (q >= breakpoint(end))
                    mid = dim - 1;
                    weight = 0;
               
               %%  Indexed search to find tick
                else
                    mid = 0;
                    weight = 0;
                    
                    %%  Data point is bigger than the last index
                    if (q < breakpoint(cache))
                        for j=cache:-1:1
                            mid = j;
                            if (q >= breakpoint(j)) && (q < breakpoint(j+1))
                                weight = (breakpoint(j+1) - q) / (breakpoint(j+1) - breakpoint(j));
                                break;
                            end                        
                        end
                    
                    %%  Data point is less than the last index
                    else
                        for j=cache:dim-1
                            mid = j;
                            if (q >= breakpoint(j)) && (q < breakpoint(j+1))
                                weight = (breakpoint(j+1) - q) / (breakpoint(j+1) - breakpoint(j));
                                break;
                            end                        
                        end
                    end
                end
                
                this.cacheList(i) = mid;
                indices(i) = mid;
                weights(i) = weight;
            end
            
            for bitstr=0:2^N-1;
                temp = bitand(bitstr, pow_array);
                factor = prod(ifelse(temp > 0, weights, 1-weights));
                
                if factor > -1e-15
				    result = result + factor * database(sub2ind(dimList,  mat2cell(ifelse(temp > 0, indices, indices + 1), 1, o){:}));
                end
            end
        endfunction    
        
    endmethods
endclassdef
