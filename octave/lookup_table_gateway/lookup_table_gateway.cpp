/**
 * Author: Bilge Kaan G�r�r, bkaangorur@hotmail.com, Feb 2021
 *
 * Interpolation code was retrieved and adapted from: https://github.com/parsiad/mlinterp
 *
 * TODO: Functions will be robust for wrong number of inputs.
 */

#include <iostream>
#include <vector>
#include "stdlib.h"
#include <octave/oct.h>
#include <octave/Cell.h>
using namespace std;

struct lnatord {
	static size_t mux(const size_t *nd, const size_t *indices, size_t Dimension) {
		size_t index = 0, product = 1, i = Dimension - 1;
		while (true) {
			index += indices[i] * product;
			if (i == 0) {
				break;
			}
			product *= nd[i--];
		}
		return index;
	}
};

struct rnatord {
	static size_t mux(const size_t *nd, const size_t *indices, size_t Dimension) {
		size_t index = 0, product = 1, i = 0;
		while (true) {
			index += indices[i] * product;
			if (i == Dimension - 1) {
				break;
			}
			product *= nd[i++];
		}
		return index;
	}
};

struct IndexWeightPair{
	size_t index;
	double weight;
};
struct IndexResultPair{
	size_t * index;
	double interpolatedValue;
};



IndexWeightPair findWeight(size_t nd, double * xd, double xi){
    const double x = xi;
    size_t mid;
    double weight;

    if (nd == 1 || x <= xd[0]) {
        // Data point is less than left boundary
        mid = 0;
        weight = 1.;
    }
    else if (x >= xd[nd - 1]) {
        // Data point is greater than right boundary
        mid = nd - 2;
        weight = 0.;
    }else {
        // Binary search to find tick
        size_t lo = 0, hi = nd - 2;
        mid = 0;
        weight = 0.;
        
        while (lo <= hi) {
            mid = lo + (hi - lo) / 2;
            if (x < xd[mid]) {
                hi = mid - 1;
            }
            else if (x >= xd[mid + 1]) {
                lo = mid + 1;
            }
            else {
                weight = (xd[mid + 1] - x) / (xd[mid + 1] - xd[mid]);
                break;
            }
        }
    }

    IndexWeightPair p = {mid, weight};
    return p;
}

template<typename Order = rnatord>
IndexResultPair interpn(double * database, size_t * dims, vector<double *> &breakpointList, double * query){
    // Infer dimension from arguments
    size_t dimension = breakpointList.size();

    // Compute 2^Dimension
    size_t power = 1 << dimension;

    // Perform interpolation for each point
    size_t * buffer	    = new size_t[dimension];
    double * weights	= new double[dimension];
 //   size_t * indices    = new size_t[dimension];
    
    double factor;
    double yi = 0.;

    IndexResultPair result;
    result.index = new size_t[dimension];
    for(size_t i=0; i<dimension; i++){
        IndexWeightPair p	= findWeight(dims[i], breakpointList[i], query[i]);
        result.index[i]		= p.index;
        weights[i]			= p.weight;
    }

    for (size_t bitstr = 0; bitstr < power; ++bitstr) {
        factor = 1.;
        for (size_t i = 0; i < dimension; ++i) {
            if (bitstr & (1 << i)) {
                buffer[i] = result.index[i];
                factor *= weights[i];
            } else {
                buffer[i] = result.index[i] + 1;
                factor *= 1 - weights[i];
            }
        }
        if (factor > std::numeric_limits<double>::epsilon()) {
            const size_t k = Order::mux(dims, buffer, dimension);
            yi += factor * database[k];
        }
    }
    result.interpolatedValue = yi;
    return result;
}

class LookupTable{
private:
    double * database;
    std::vector<double *> breakpointList;
    size_t N;
    size_t * dims;

public:
    LookupTable(NDArray * databaseData, Cell * breakpointListData){
        this->N = breakpointListData->numel();
        
        dims = new size_t[N];
        for(size_t i=0; i<N; i++){
            NDArray temp = (*breakpointListData)(i).matrix_value();
            dims[i] = temp.numel();
            double * ptr = new double[dims[i]];
            memcpy(ptr, temp.fortran_vec(), sizeof(double)*dims[i]);
            breakpointList.push_back(ptr);
        }
        
        database    = new double[databaseData->numel()];
        
        memcpy(database, databaseData->fortran_vec(), sizeof(double)*databaseData->numel());
    }
    
    double request(double * query){
        IndexResultPair result = interpn(   &(database[0]),
                                            &(dims[0]),
                                            breakpointList, 
                                            &(query[0]));

        return result.interpolatedValue;
    }
    
    void print(){
        size_t elementCount = 1;
        std::cout << "Breakpoint List [" << N << "]:\n  ";
        for(size_t i=0; i<N; i++){
            double * breakpoint = breakpointList[i];
            for(size_t j=0; j<dims[i]; j++){
                std::cout << breakpoint[j] << " ";
            }
            std::cout << "\n  ";
            elementCount *= dims[i];
        }
        
        std::cout << "\nDatabaseList [" << elementCount << "]:\n  ";
        for(size_t i=0; i</*elementCount*/1000; i++){
            std::cout << database[i] << " ";
        }
        std::cout << "\n";
    }
};

std::map<std::string, LookupTable *> lookupTableMap;


DEFUN_DLD (createLookupTable, args, , "creates an N-D lookup table"){
    NDArray databaseData    = args(0).matrix_value();
    Cell breakpointListData = args(1).cell_value();
    std::string name        = args(2).string_value();
    
    LookupTable * lookupTable = new LookupTable(&databaseData, &breakpointListData);
    lookupTableMap[name] = lookupTable;
    
    return ovl(1);
}

DEFUN_DLD (printLookupTable, args, , "prints a lookup table"){
    std::string name = args(0).string_value();
    LookupTable * lookupTable = lookupTableMap[name];
    return ovl(1);
}

DEFUN_DLD (requestLookupTable, args, , "requests some values from a lookup table"){
    std::string name    = args(0).string_value();
    NDArray queryData   = args(1).matrix_value();
    double * query      = queryData.fortran_vec();
    
    LookupTable * lookupTable = lookupTableMap[name];
    double result = lookupTable->request(query);    
    return ovl(result);
}
