%%  Author: Bilge Kaan G�r�r, bkaangorur@hotmail.com, Feb 2021.
%%  Interpolation operation was adapted from the implementation in https://github.com/parsiad/mlinterp

classdef LookupTable < handle
    properties
        database;               %   Previously computed/measured data 
        breakpointList = {};    %   The list of breakpoint data vectors
        pow_array;
        dimList;                %   The list of dimensions of each breakpoint 
        dim = 0;
    endproperties
    
    methods
        function createLookupTable(this, database, varargin)
            this.database = database;
            this.dim      = length(varargin);
            for i=1:this.dim
                this.breakpointList{i} = varargin{i};
                this.dimList(i) = length(varargin{i}(:));
            endfor
        endfunction
        
        function result = request(this, varargin)
            N               = this.dim;
            z               = zeros(1, N);
            o               = ones(1, N);
            indices         = z;
            weights         = z;
            result          = 0;
            
            dimList         = this.dimList;
            database        = this.database;
            breakpointList  = this.breakpointList;
            pow_array       = 2.^([1:N]-1);
            
            for i=1:N
                bb = breakpointList{i};
                indices(i) = lookup(bb, varargin{i});
                if indices(i) >= dimList(i)
                    indices(i) = indices(i)-1;
                end
                weights(i) = (bb(indices(i)+1) - varargin{i}) / (bb(indices(i)+1) - bb(indices(i)));
            end
     
            for bitstr=0:2^N-1;
                temp = bitand(bitstr, pow_array);
                factor = prod(ifelse(temp > 0, weights, 1-weights));
                
                if factor > -1e-15
				    result = result + factor * database(sub2ind(dimList,  mat2cell(ifelse(temp > 0, indices, indices + 1), 1, o){:}));
                end
            end
        endfunction    
        
    endmethods
endclassdef
