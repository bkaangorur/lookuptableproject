/*
 * TemplatedLookupTable.h
 *
 *  Created on: 30 Oct 2020
 *      Author: Bilge Kaan Görür <bkaangorur@hotmail.com>
 *
 *      TODO:
 *      	1. cached and binary search will be separated into different functions
 *      	2. LookupTable class will be a template class
 *      	3. Performance optimization.
 *      	4. LookupTable constructor will be more user-friendly for N-dimensional lookup tables
 *      		to avoid type conversions in driver code.
 */

#ifndef LOOKUPTABLE_H_
#define LOOKUPTABLE_H_

#include <vector>
#include <limits>
#include <sstream>

enum ExtrapolationType {
	CLIP,
	NO_EXTRAPOLATION
};

struct lnatord {
	template<typename Index>
	static Index mux(const Index *nd, const Index *indices, Index Dimension) {
		Index index = 0, product = 1, i = Dimension - 1;
		while (true) {
			index += indices[i] * product;
			if (i == 0) {
				break;
			}
			product *= nd[i--];
		}
		return index;
	}
};

struct rnatord {
	template<typename Index>
	static Index mux(const Index *nd, const Index *indices, Index Dimension) {
		Index index = 0, product = 1, i = 0;
		while (true) {
			index += indices[i] * product;
			if (i == Dimension - 1) {
				break;
			}
			product *= nd[i++];
		}
		return index;
	}
};

template<typename T>
struct IndexWeightPair{
	size_t index;
	T weight;
};


template<typename T=double>
class TemplatedLookupTable{
protected:
	std::string dataName;
	std::vector<T> dataTable;                     // Data table
	std::vector<T *> breakpointList;              // Breakpoints for each dimension
	std::vector<std::string> breakpointNameList;  // Name of each dimension
	std::vector<size_t> dims;                     // Dimension list of breakpoints
	size_t N;                                     // Lookup table dimension
	ExtrapolationType extrapolationType;
	std::vector<size_t> cacheIndex;               // List of cached indices for each dimension

	//	Indexed search is applied
	IndexWeightPair<T> findWeight(size_t nd, T * xd, T xi, size_t breakpointIndex){
		size_t previousIndex = cacheIndex[breakpointIndex];
		const T x = xi;

		size_t mid;
		T weight;

		if (nd == 1 || x <= xd[0]) {
			// Data point is less than left boundary
			mid = 0;
			weight = 1.;
		}
		else if (x >= xd[nd - 1]) {
			// Data point is greater than right boundary
			mid = nd - 2;
			weight = 0.;
		}else {
			// Indexed search to find tick
			mid = 0;
			weight = 0.;

			//	Data point is bigger than the last index
			if(x < xd[previousIndex]){
				for(size_t i=previousIndex; i>=0; i--){
					mid = i;
					if(x >= xd[i] && x < xd[i+1]){
						weight = (xd[i + 1] - x) / (xd[i + 1] - xd[i]);
						break;
					}
				}
			}

			//	Data point is less than the last index
			else{
				for(size_t i=previousIndex; i<nd-1; i++){
					mid = i;
					if(x >= xd[i] && x < xd[i+1]){
						weight = (xd[i + 1] - x) / (xd[i + 1] - xd[i]);
						break;
					}
				}
			}
		}

		IndexWeightPair<T> p = {mid, weight};
		cacheIndex[breakpointIndex] = mid;
		return p;
	}

	//	Binary search is applied
	IndexWeightPair<T> findWeight(size_t nd, T * xd, T xi){
		const T x = xi;

		size_t mid;
		T weight;

		if (nd == 1 || x <= xd[0]) {
			// Data point is less than left boundary
			mid = 0;
			weight = 1.;
		}
		else if (x >= xd[nd - 1]) {
			// Data point is greater than right boundary
			mid = nd - 2;
			weight = 0.;
		}else {
			// Binary search to find tick
			size_t lo = 0, hi = nd - 2;
			mid = 0;
			weight = 0.;
			while (lo <= hi) {
				mid = lo + (hi - lo) / 2;
				if (x < xd[mid]) {
					hi = mid - 1;
				}
				else if (x >= xd[mid + 1]) {
					lo = mid + 1;
				}
				else {
					weight = (xd[mid + 1] - x) / (xd[mid + 1] - xd[mid]);
					break;
				}
			}
		}

		IndexWeightPair<T> p = {mid, weight};
		return p;
	}

	template<typename Order = rnatord, typename Index>
	T interp(const T *yd, const Index *nd, std::vector<T*> &breakpointList, T* query) {
		// Infer dimension from arguments
		Index Dimension = N;

		// Compute 2^Dimension
		Index Power = 1 << Dimension;

		// Perform interpolation for each point
		Index * buffer	= new Index[Dimension];
		Index * indices	= new Index[Dimension];
		T * weights		= new T[Dimension];
		T factor;
		T yi = 0.;

		std::vector<IndexWeightPair<T>> indexWeightList;
		for(size_t i=0; i<N; i++){
//			IndexWeightPair<T> p	= findWeight(nd[i], breakpointList[i], query[i], i);		//	Apply cached search
			IndexWeightPair<T> p	= findWeight(nd[i], breakpointList[i], query[i]);			//	Apply binary search
			indices[i]				= p.index;
			weights[i]				= p.weight;
		}

		for (Index bitstr = 0; bitstr < Power; ++bitstr) {
			factor = 1.;
			for (Index i = 0; i < Dimension; ++i) {
				if (bitstr & (1 << i)) {
					buffer[i] = indices[i];
					factor *= weights[i];
				} else {
					buffer[i] = indices[i] + 1;
					factor *= 1 - weights[i];
				}
			}
			if (factor > std::numeric_limits<T>::epsilon()) {
				const Index k = Order::template mux<Index>(nd, buffer, Dimension);
				yi += factor * yd[k];
			}
		}

		delete [] indices;
		delete [] weights;
		delete [] buffer;
		return yi;
	}

	void getData(T* query){}

	template <typename... Args>
	void getData(T* query, T var1, Args... var2){
		size_t index = sizeof...(Args);
	    query[N-index-1] = var1;
	    getData(query, var2...) ;
	}

	void dimCounterForVector(std::vector<double> &v){
		std::stringstream ss;
		ss << "breakpoint_matrix_" << N;
		breakpointNameList.push_back(ss.str());
		dims.push_back(v.size());
		T * breakpointData = new T[v.size()];
		memcpy(breakpointData, v.data(), v.size() * sizeof(T));
		breakpointList.push_back(breakpointData);
		cacheIndex.push_back(0);

		this->N++;
	}

	template <typename... Args>
	void dimCounterForVector(std::vector<double> &v, Args... dataList){
		std::stringstream ss;
		ss << "breakpoint_matrix_" << N;
		breakpointNameList.push_back(ss.str());

		dims.push_back(v.size());
		T * breakpointData = new T[v.size()];
		memcpy(breakpointData, v.data(), v.size() * sizeof(T));
		breakpointList.push_back(breakpointData);
		cacheIndex.push_back(0);

		this->N++;
		dimCounterForVector(dataList...);
	}

public:
	template <typename... Args>
	TemplatedLookupTable(std::vector<double> &database, std::vector<double> &breakpoint1, Args... otherBreakpoints){
		this->N			= 0;
		this->dataName	= dataName;

		dimCounterForVector(breakpoint1, otherBreakpoints...);

		this->extrapolationType = ExtrapolationType::NO_EXTRAPOLATION;

		//	Read database
		this->dataTable = database;
	}

	virtual ~TemplatedLookupTable(){
		for(size_t i=0; i<breakpointList.size(); i++){
			T * ptr = breakpointList[i];
			delete []ptr;
		}

		breakpointList.clear();
		dataTable.clear();
		breakpointNameList.clear();
		dims.clear();
		cacheIndex.clear();

		breakpointList.shrink_to_fit();
		dataTable.shrink_to_fit();
		breakpointNameList.shrink_to_fit();
		dims.shrink_to_fit();
		cacheIndex.shrink_to_fit();
	}

	void setExtrapolationType(ExtrapolationType e){
		this->extrapolationType = e;
	}

	template <typename... Args>
	T request(T var1, Args... var2){
		T * query = new T[N];
		getData(query, var1, var2...);

		T result = interp(dataTable.data(), dims.data(), breakpointList, query);

		delete [] query;
		return result;
	}

	template <typename... Args>
	T request_row_major(T var1, Args... var2){
		T * query = new T[N];
		getData(query, var1, var2...);

		T result = interp<lnatord>(dataTable.data(), dims.data(), breakpointList, query);

		delete [] query;
		return result;
	}

	std::string toString(){
		std::stringstream ss;
		ss << dataName << " lookup table with " << N << " dimension [";

		for(size_t i=0; i<dims.size(); i++){
			if(i != dims.size()-1)
				ss << dims[i] << ", ";
			else
				ss << dims[i];
		}
		ss << "]" << "\n";

		for(size_t i=0; i<breakpointNameList.size(); i++){
			ss << "  - " << dims[i] << " elements in " << breakpointNameList[i] << "\n";
		}
		ss << "\n";

		return ss.str();
	}
};

//	For ease of use
typedef TemplatedLookupTable<> LookupTable;

#endif /* LOOKUPTABLE_H_ */
