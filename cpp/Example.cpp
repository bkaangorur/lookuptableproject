/*
 * Example.cpp
 *
 *  Created on: 10 Jul 2021
 *      Author: Bilge Kaan Görür <bkaangorur@hotmail.com>
 */

#include <armadillo>
#include <LookupTable.h>
using namespace arma;


int main(){
	srand((unsigned)time(NULL));

	//	Read precomputed lookup data from csv files
	mat temperature_table_mat, cpu_breakpoints_mat, memory_breakpoints_mat;
	temperature_table_mat.load("database/temperature_table.csv");
	cpu_breakpoints_mat.load("database/cpu_breakpoints.csv");
	memory_breakpoints_mat.load("database/memory_breakpoints.csv");

	//	Convert arma::mat variables to std::vector to make them compatible with lookup table class
	std::vector<double> temperature_table, cpu_breakpoints, memory_breakpoints;
	temperature_table  = conv_to<std::vector<double>>::from(vectorise(temperature_table_mat));
	cpu_breakpoints    = conv_to<std::vector<double>>::from(cpu_breakpoints_mat);
	memory_breakpoints = conv_to<std::vector<double>>::from(memory_breakpoints_mat);

	//	Create a lookup table object
	LookupTable * temperature_LUT = new LookupTable(temperature_table, memory_breakpoints, cpu_breakpoints);

	double min_memory_usage    = 4;
	double max_memory_usage    = 32;
	double min_cpu_usage       = 1;
	double max_cpu_usage       = 100;

	//	Make random requests from the lookup table.
	for(size_t i=0; i<100; i++){
		double current_memory_usage = min_memory_usage + (max_memory_usage-min_memory_usage) * ((double)rand()/(double)RAND_MAX);
		double current_cpu_usage    = min_cpu_usage    + (max_cpu_usage-min_cpu_usage)       * ((double)rand()/(double)RAND_MAX);

		temperature_LUT->request(current_memory_usage, current_cpu_usage);
	}

	delete temperature_LUT;

	return 0;
}

